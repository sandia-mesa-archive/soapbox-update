#!/usr/bin/env bash
# Soapbox FE Updater Script
# Programmer: Sean King
# Copyright: 2020 Sandia Mesa Animation Studios and other contributors
# License: WTFPL
#
# I'm sick of having to make my folks wait a long time while I ship an update.
# So, I thought I would develop this little bash script to make it automatically
# be done after I upload the new build to another directory in my server.
#
# All you do is git clone this project to the Pleroma user’s home directory and set the
# script permssions properly. After that, go to the project folder under the Pleroma user's
# home directory and run sudo -Hu pleroma ./soapbox-update.sh. And voila! You're good to go!
#
# Note: If you want to get the latest develop build instead of the stable build
# like I do, run sudo -Hu pleroma ./soapbox-update.sh develop.

# First, we'll determine whether to download the latest stable or develop build.
if [ "$1" = "develop" ]; then
  version="develop"
  downloading_echo="the latest develop build of Soapbox FE"
  unzipping_echo="develop build"
else
  version=$(git ls-remote --tags https://gitlab.com/soapbox-pub/soapbox-fe.git | grep -o "refs/tags/v[0-9]*\.[0-9]*\.[0-9]*" | sort -rV | head -1 | grep -o '[^\/]*$')
  downloading_echo="Soapbox FE ${version}"
  unzipping_echo=$downloading_echo
fi

# Then, we'll get the latest develop or stable build (depending on if the user specified develop in the execution command) from GitLab.
echo "Downloading ${downloading_echo} from GitLab..."; mkdir .tmp; curl -L https://gitlab.com/soapbox-pub/soapbox-fe/-/jobs/artifacts/${version}/download?job=build-production -o .tmp/soapbox-fe.zip --progress-bar

# Now, we'll uninstall the current stuff in the FE bundle directory.
if [ -d "/opt/pleroma/instance/static/frontends/soapbox-fe/${version}/packs" ] && [ -d "/opt/pleroma/instance/static/frontends/soapbox-fe/${version}/sounds" ] && [ -d "/opt/pleroma/instance/static/frontends/soapbox-fe/${version}/instance" ] && [ -f "/opt/pleroma/instance/static/frontends/soapbox-fe/${version}/index.html" ]; then
  echo "Uninstalling existing Soapbox FE bundle..."; rm -R /opt/pleroma/instance/static/frontends/soapbox-fe/${version}/*
fi

# Finally, we'll unzip soapbox-fe.zip to the FE bundle directory.
echo "Installing new Soapbox FE bundle..."; busybox unzip .tmp/soapbox-fe.zip -o -d .tmp > /dev/null; cp -r .tmp/static/* /opt/pleroma/instance/static/frontends/soapbox-fe/${version}

# Now that it's all over, let's remove the .tmp dir.
echo "Deleting temp files..."; rm -R .tmp

echo "All done."
