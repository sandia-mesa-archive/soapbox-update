# Soapbox FE Updater Script

> :warning: **This repo has been deprecated. Use the ``mix pleroma.frontend install`` task in Pleroma to [install and update frontends](https://docs.pleroma.social/backend/administration/CLI_tasks/frontend/) instead.**

I'm sick of having to make my folks wait a long time while I ship an update. So, I thought I would develop this little bash script to make it automatically be done after I upload the new build to another directory in my server.

All you do is git clone this project to the Pleroma user's home directory and set the script permssions properly, which can be done by running the following:
```
# Download the Soapbox FE Updater script to the Pleroma user's home directory. Note that this command can also be used to get the latest version of the script.
sudo -Hu pleroma git clone https://code.sandiamesa.com/traboone/soapbox-update /var/lib/pleroma/soapbox-update

# Set the Soapbox FE Updater script to be executable by the Pleroma user.
chmod u+x /var/lib/pleroma/soapbox-update/soapbox-update.sh
```

After that, switch to the project folder under the Pleroma user's home directory by running ``cd /var/lib/pleroma/soapbox-update``. Then finally, run ``sudo -Hu pleroma ./soapbox-update.sh``. And voila! You're good to go!

**Note:** If you want to get the latest develop build instead of the stable build like I do, run ``sudo -Hu pleroma ./soapbox-update.sh develop``.

## License

(C) 2020 Sandia Mesa Animation Studios and other contributors
<br>This program is licensed under the [Do What The Fuck You Want To Public License (WTFPL)](LICENSE.md).
