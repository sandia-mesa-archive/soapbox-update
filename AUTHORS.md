# Authors

Soapbox FE Updater Script is available on Sandia Mesa’s self-hosted [Gitea](https://code.sandiamesa.com/traboone/soapbox-update) and is provided thanks to the work of the following contributors:

* Sean King - <sean.king@sandiamesa.com>
